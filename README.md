The office of Samuel E. Gabriel and Associates is a full service law office. Our attorneys practice in the areas of personal injury, slip and fall, accidents, workers comp, car accident and other matters. We will do everything to keep you fully informed and respond to your phone calls promptly.

Address: 801 Pacific Avenue, Long Beach, CA 90813, USA

Phone: 562-436-9292

Website: http://gabriel-law.com/
